<?php declare (strict_types=1);
/**
 * @package connectDetect
 * @version 0.0.1
 * PHP VERSION >= 8.0
 */
/*
Plugin Name: Connect Detect
Plugin URI: 
Description: Journalise les connexions et tentatives de connexions de wp. Menu -> réglage -> Connect Detect
Author: legite.org
Version: 0.0.1
Author URI: https://legite.org
*/

define ('PLUGIN_CONNECT_DETECT_ROOT', plugin_dir_path( __FILE__ )); // finit par DIRECTORY_SEPARATOR
define ('PLUGIN_CONNECT_DETECT_URI' , plugin_dir_url ( __FILE__ )); // finit par DIRECTORY_SEPARATOR
//echo __FILE__;
use function org\legite\libs\inspectVars         as inspectVars;
use function org\legite\libs\inspectVarsOrigine  as inspectVarsOrigine;
use function org\legite\libs\showException       as showException;
use          org\legite\libs\GestLog             as GestLog;
use function org\legite\libs\print_code          as print_code;

//const PLUGIN_CONNECT_DETECT_DIR=__DIR__;
define ('PLUGIN_CONNECT_DETECT_MENU_URL', get_bloginfo( 'url', 'raw').'/wp-admin/options-general.php?page=connectDetect');

/**
 * AUTHENTIFICATION - LOGIN
 *  themes/apc/libs/legite-wp-log/legite-wp-log.php
 * https://usersinsights.com/wordpress-user-login-hooks/

 * Description:
 * Utilise le hook de connexion WP pour intercepter les demande de connexion et les journlaiser dans une table
 
 * Installation:

 * Ajouter un callback (sous forme de méthode d'objet) de hook 
 * https://stackoverflow.com/questions/55720528/how-to-call-a-class-method-as-a-callback-function-in-wordpress-custom-endpoint

 * structure de la table 
 * Nom 	              Type        Interclassement 	Attributs 	        Null 	 Valeur par défaut    Extra
 * id                 Primaire     bigint(20) 		    UNSIGNED 	     Non 	  Aucun(e) 		      AUTO_INCREMENT
 * ip                 char(15)     utf8mb4_general_ci 	                 Non 				
 * date_de_connexion  datetime 	                                         Non      current_timestamp()
 * user_id            int                                                Oui
 * pseudo
 * isConnect_ok        boolean                                           Non
 */

/******************************************
 * function public pour le callback du hook
*******************************************/
function legite_wp_log_authenticate($user, $username, $password){
    global $legite_wp_login;
    return $legite_wp_login->authenticate($user, $username, $password); // return $user
    }


/*********
 * CLASS *
**********/
class Legite_wp_login{

    private $mysqli=null;
    private $table_nom='legite_connectDetect';
    function __construct(){

        /* AJOUT DU HOOK */
        add_filter('authenticate', 'legite_wp_log_authenticate', 25, 3);
    }


    /*********
     * INDEX *
    **********/
    public function index(){
        echo '<h1>Plugin Connect Detect</h1>';

        // # INSTALLATION  # //
        if (! $this->isTableInstall()){
            echo '<h2>Installation</h2>';
            echo "La table n'existe pas -> création";
            $this->createTable();
        }


        // # MENU  # //
        $_menu=(isset($_GET['menu']))?$_GET['menu']:'connexions_fail';
        echo '<h2>Journaux</h2>';
        echo '<ul id="plugin_connectDetect_menus"><li>Connexions:</li>';

        echo '<li><a href="'.PLUGIN_CONNECT_DETECT_MENU_URL.'&amp;menu=connexions">Toutes</a></li>';
        echo '<li><a href="'.PLUGIN_CONNECT_DETECT_MENU_URL.'&amp;menu=connexions_ok">ok</a></li>';
        echo '<li><a href="'.PLUGIN_CONNECT_DETECT_MENU_URL.'&amp;menu=connexions_fail">fails</a></li>';

        echo '<li>par IP:</li>';
        echo '<li><a href="'.PLUGIN_CONNECT_DETECT_MENU_URL.'&amp;menu=connexionsIP">Toutes</a></li>';
        echo '<li><a href="'.PLUGIN_CONNECT_DETECT_MENU_URL.'&amp;menu=connexionsIP_ok">ok</a></li>';
        echo '<li><a href="'.PLUGIN_CONNECT_DETECT_MENU_URL.'&amp;menu=connexionsIP_fail">fails</a></li>';
        echo '</ul>';
    
        switch ($_menu){
            case 'connexions':      $this->showConnexions(null);     break;
            case 'connexions_ok':   $this->showConnexions(true);     break;
            case 'connexions_fail': $this->showConnexions(false);     break;
            case 'connexionsIP':      $this->showConnexion_connectByIP(null);     break;
            case 'connexionsIP_ok':   $this->showConnexion_connectByIP(true);     break;
            case 'connexionsIP_fail': $this->showConnexion_connectByIP(false);     break;
        }
    
    } // inddex()



    /*****************************
     * DB: CONNEXION/DECONNEXION *
    ******************************/
    public function connectDB():bool{

        /** * CONNEXION A LA BASE DE DONNEES * **/
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

        try{
            $this->mysqli = mysqli_init();
            if (!$this->mysqli) {
                echo 'Erreur lors de la connexion avec la DB';
                return false;
            }
        }
        catch (Exception $e){
            showException($e, 'mysqli');
            return false;
        }

        /*
        * NE PAS ACTIVER
        if (!$this->mysqli->options(MYSQLI_INIT_COMMAND, 'SET AUTOCOMMIT = 0')){
            echo 'Setting MYSQLI_INIT_COMMAND failed';
            return false;
        }
        */

        if (!$this->mysqli->options(MYSQLI_OPT_CONNECT_TIMEOUT, 5)){
            echo 'Setting MYSQLI_INIT_COMMAND failed';
            return false;
        }

        if (! $this->mysqli->real_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME)){
            show($this->mysqli);
            return false;
        }
        return true;

    }

    public function deconnectDB(){

        /** * DECONNEXION DE LA BASE DE DONNEES * **/
        if ( $this->mysqli != null){
            $this->mysqli->close();
            $this->mysqli= null;
        }
    }


    /**********************
     * DB: TABLE: INSTALL *
    ***********************/
    public function isTableInstall(): bool{

        // # Connexion à la base de données # //
        if (! $this->connectDB() ){
            echo 'Connexion à la base de données impossible!<br>';
            return false;
        }

        // # Requete # //
        $_requeteSQL= "SELECT COUNT(id) FROM `$this->table_nom`;;";
        try{
            if ($_result = $this->mysqli->query($_requeteSQL)) {
                // Si la table existe pas cela provoque une exception qui return false
                $_result->free_result();
                $this->deconnectDB();
                return true; //Pas d'exception, donc c'est ok
            }
        }

        catch(ArgumentCountError $e){
            //echo __FILE__.':'.__LINE__.'<br>';
            //showException($e,'Exception ArgumentCountError sql');
        }

        catch(Exception $e){
            //echo __FILE__.':'.__LINE__.'<br>';
            //showException($e,'Exception sql');
        }    


        $this->deconnectDB();
        return false;
    } // isTableInstall()


    public function createTable():bool{

        // # Connexion à la base de données # //
        if (! $this->connectDB() ){
            echo 'Connexion à la base de données impossible!<br>';
            return false;
        }

        // # Requete # //
        $_requeteSQL= "CREATE TABLE `$this->table_nom` (
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `date_de_connexion` datetime DEFAULT current_timestamp(),
            `ip` varchar(35) DEFAULT NULL,
            `pseudo` varchar(50) DEFAULT NULL,
            `isConnect_ok` tinyint(1) NOT NULL DEFAULT 0,
            `Id_APC_utilisateur` int(11) DEFAULT NULL,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;";
        try{
            if ($this->mysqli->query($_requeteSQL)) {
                // Le resultat du query est sans intérêt
                $this->deconnectDB();
                return true; //Pas d'exception, donc c'est ok
            }
        }

        catch(ArgumentCountError $e){
            //echo __FILE__.':'.__LINE__.'<br>';
            showException($e,'Exception ArgumentCountError sql');
        }

        catch(Exception $e){
            echo __FILE__.':'.__LINE__.'<br>';
            showException($e,'Exception sql');
        }    

        $this->deconnectDB();
        return false;
    }


    /*************************
     * HOOK: AUTHENTIFICATION *
    **************************/

    /**
     * return false si erreur à la connexion de la DB
     * return $user si réussite de connexion à la db
     * $user peut ne pas exister comme user WP
     */
    public function authenticate($user, $username, $password){

        // # Connexion à la base de données # //
        if (! $this->connectDB() ){
            echo 'Connexion à la base de données impossible!<br>';
            return false;
        }

        if (empty($user->errors)){
            // Pas d'erreur: journalisation d'une connexion réussie
            echo 'erreur détéctée.<br>';
            $this->insertConnexion(isConnect_ok:true);
        }
        else{
            // Erreur: journalisation d'une connexion fails
            $this->insertConnexion(isConnect_ok:false);
        }

        // # Fermeture de  la connexion à labase de données # //
        $this->deconnectDB();

        return $user;
    }


    /**********************************
     * JOURNAUX DE CONNEXION: LECTURE *
    ***********************************/

    /**
     * Affiche toutes les lignes de la table des connexions la demande de connexion
     */
    public function showConnexions(bool $isConnect_ok=null){

        // # Connexion à la base de données # //
        if (! $this->connectDB() ){
            echo 'Connexion à la base de données impossible!<br>';
            return false;
        }

        $_where='';   // null: toutes connexions
        if ($isConnect_ok === null){
            echo '<h3>Connexions (toutes)</h3>';
        }
        if ($isConnect_ok === true){
                echo '<h3>Connexions (réussies)</h3>';
                $_where='WHERE isConnect_ok=1';
        }
        if ($isConnect_ok === false){
                echo '<h3>Connexions (fails)</h3>';
                $_where='WHERE isConnect_ok=0';
        }

        $_requeteSQL= "SELECT * FROM `$this->table_nom` $_where ORDER BY  	date_de_connexion DESC;";
        try{
            if ($_result = $this->mysqli->query($_requeteSQL)) {
                $this->showConnexion_table($_result);
                $_result->free_result();
            }
        }

        catch(ArgumentCountError $e){
            //echo __FILE__.':'.__LINE__.'<br>';
            showException($e,'Exception ArgumentCountError sql');
        }

        catch(Exception $e){
            //echo __FILE__.':'.__LINE__.'<br>';
            showException($e,'Exception sql');
        }    
        // # Fermeture de  la connexion à labase de données # //
        $this->deconnectDB();

    } //showConnexions()


    /******************************
     * AFFICHAGE CONNEXION PAR IP *
    *******************************/

    /**
     * Affiche toutes les IP (reussies+fails) selon leur nombre
     * @param format
     *  0: toutes IP connexions resussies+ fails
     *  1: connexions réussies
     *  2: connexions fails
     */

    public function showConnexion_connectByIP(?bool $isConnect_ok=null){

        // # Connexion à la base de données # //
        if (! $this->connectDB() ){
            echo 'Connexion à la base de données impossible!<br>';
            return false;
        }

        $_where='';   // null: toutes connexions
        if ($isConnect_ok === null){
            echo '<h3>Connexions (toutes)</h3>';
        }
        if ($isConnect_ok === true){
                echo '<h3>Connexions (réussies)</h3>';
                $_where='WHERE isConnect_ok=1';
        }
        if ($isConnect_ok === false){
                echo '<h3>Connexions (fails)</h3>';
                $_where='WHERE isConnect_ok=0';
        }

        $_requeteSQL= "SELECT  COUNT(ip) as ipNb, ip FROM `$this->table_nom` $_where GROUP BY ip ORDER BY ipNb DESC,ip ASC;";

        try{
            if ($_result = $this->mysqli->query($_requeteSQL)) {
                $this->showConnexion_tableIP($_result);
                $_result->free_result();
            }
        }

        catch(ArgumentCountError $e){
            //echo __FILE__.':'.__LINE__.'<br>';
            showException($e,'Exception ArgumentCountError sql');
        }

        catch(Exception $e){
            //echo __FILE__.':'.__LINE__.'<br>';
            showException($e,'Exception sql');
        }    

        // # Fermeture de  la connexion à labase de données # //
        $this->deconnectDB();

    } //showConnexion_reussie()


    /**
     * Gestion d'affiche d'une <table> avec les données des journaux
    */
    function showConnexion_table($sql_result){?>
        <table id="showConnexion_table">
            <thead>
                <tr>
                <th>date</th>
                <th>IP</th>
                <th>pseudo</th>
                <th>isConnect_ok</th>
                <th>wp_user_id</th>
                </tr>
            </thead>
            <tboby>
                <?php
                while ($_row = $sql_result->fetch_row()) {
                    $_out='';
                    //printf ("%s (%s)\n", $_row[0], $_row[1]);
                    //inspectVars('_row',$_row);
                    $_out.='<tr>';
                    //$_out.='<td>'.$_row[0].'</td>'; // id de la table
                    $_out.='<td>'.$_row[1].'</td>';
                    $_out.='<td>'.$_row[2].'</td>';
                    $_out.='<td class="center">'.$_row[3].'</td>';
                    $_out.='<td class="center">'.$_row[4].'</td>';
                    $_out.='<td class="center">'.$_row[5].'</td>';
                    $_out.='</tr>';
                    echo $_out;
                }
                ?>
            </tboby>
        </table>
        <hr>
    <?php } // showConnexion_table()
    

    /**
     * Gestion d'affiche d'une <table> avec les données des journaux
    */
    function showConnexion_tableIP($sql_result){?>
        <table>
            <thead>
                <tr>
                <th>nb</th>
                <th>IP</th>
                </tr>
            </thead>
            <tboby>
                <?php
                while ($_row = $sql_result->fetch_row()) {
                    $_out='';
                    //printf ("%s (%s)\n", $_row[0], $_row[1]);
                    //inspectVars('_row',$_row);
                    $_out.='<tr>';
                    $_out.='<td>'.$_row[0].'</td>'; // id de la table
                    $_out.='<td>'.$_row[1].'</td>';

                    $_out.='</tr>';
                    echo $_out;
                }
                ?>
            </tboby>
        </table>
        <hr>
    <?php } // showConnexion_tableIP()


    /************************************
     * AJOUT DE LA CONNEXION DANS LA DB *
    *************************************/
    /**
     * Inscrit dans la table des connexions la demande de connxion
     */
    public function insertConnexion( bool $isConnect_ok=false){

        // # Connexion à la base de données # //
        if (! $this->connectDB() ){
            echo 'Connexion à la base de données impossible!<br>';
            return false;
        }

        $_userIP=$_SERVER['REMOTE_ADDR'];
        $_userId=get_current_user_id();
        $_wp_post_login=$_POST['log']??'';    // champ login du formulaire de connexion
        $_isConnect_integer=$isConnect_ok?1:0;

        // Journaliser la demande uniquement si login fournis
        if ($_wp_post_login === ''){
            return;
        }

        $_requeteSQL= "INSERT INTO `$this->table_nom` (`ip`, `Id_APC_utilisateur`, `pseudo`, `isConnect_ok`      )";
        $_requeteSQL.="    VALUES                         (  ? ,         ?           ,     ?   , $_isConnect_integer );";

        $_rp=$this->mysqli->prepare($_requeteSQL);

        try{
            $_rp->bind_param('sis',$_userIP,$_userId, $_wp_post_login);
            $_rp->execute();
            // https://www.php.net/manual/fr/class.mysqli-result.php
            $result = $_rp->get_result();
        }
        catch(ArgumentCountError $e){
            //echo __FILE__.':'.__LINE__.'<br>';
            showException($e,'Exception ArgumentCountError sql');
        }

        catch(Exception $e){
            //echo __FILE__.':'.__LINE__.'<br>';
            showException($e,'Exception sql');
        }    
        
        // # Fermeture de  la connexion à labase de données # //
        $this->deconnectDB();
    } //insertConnexion()

} //class


/********
 * MAIN *
*********/


/*********************************
 * HOOK D'INSTALLATION DU PLUGIN *
**********************************/
add_action( 'admin_enqueue_scripts', 'connectDetect_css_load_js_css' );
function connectDetect_css_load_js_css(){
    wp_enqueue_style( 'connectDetect_css', PLUGIN_CONNECT_DETECT_URI . 'connectDetect.css' );
    wp_enqueue_script( 'connectDetect_js', PLUGIN_CONNECT_DETECT_URI . 'connectDetect.js', array(), '1.0.0', false );
}


/*******************
 * INITILALISATION *
*********************/
$legite_wp_login=new Legite_wp_login();


/**********************
 * HOOK DE MENU ADMIN *
*********************/


// * MENU CONNECT TOUTES * //
add_action('admin_menu', 'connectDetect_addMenu');
function connectDetect_addMenu(){
    $_page_title='Plugin de détéction de connexions';
    $_menu_title='Connect Detect';
    $_capability='administrator';
    $_menu_slug='connectDetect';
    $_callback='connectDetect_callback';
    $_position=1;
    add_submenu_page( 'options-general.php', $_page_title, $_menu_title, $_capability, $_menu_slug, $_callback, $_position );
}
function connectDetect_callback(){
    $legite_wp_login=new Legite_wp_login();
    $legite_wp_login->index();
}
